
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
<title>Opulent Coming Soon Widget Flat Responsive Widget Template :: w3layouts</title>
<!-- Meta Tags -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<meta name="keywords" content="Opulent Coming Soon Widget Responsive, Login form web template, Sign up Web Templates, Flat Web Templates, Login signup Responsive web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<!-- //Meta Tags -->
<!-- Fonts -->
<link href="//fonts.googleapis.com/css?family=Josefin+Sans:300,400,600,700" rel="stylesheet">
<link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!-- //Fonts -->
<!-- Style Sheets -->
<link href="<?php echo base_url('assets/landingpage/css/style.css')?>" rel="stylesheet" type="text/css" media="all" />
<!-- //Style Sheets -->
</head>
<body>
	<div class="content">
			<div class="main agile">
			<h1>Opulent Coming Soon Widget</h1>
			<p class="para-w3ls">We'll be here soon with something smashing</p>
				<div class="clock w3agile">
            <div class="column days">
                <div class="timer" id="days"></div>
                <div class="text">DAYS</div>
            </div>
           
            <div class="column">
                <div class="timer" id="hours"></div>
                <div class="text">HOURS</div>
            </div>
           
            <div class="column">
                <div class="timer" id="minutes"></div>
                <div class="text">MINUTES</div>
            </div>
           
            <div class="column">
                <div class="timer" id="seconds"></div>
                <div class="text">SECONDS</div>
            </div>
        </div>
		 <!-- Custom-JavaScript-File-Links -->
        <script type="text/javascript" src="<?php echo base_url('assets/landingpage/js/jquery-2.1.4.min.js')?>"></script>
		<script type="text/javascript" src="<?php echo base_url('assets/landingpage/js/moment.js')?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/landingpage/js/moment-timezone-with-data.js')?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/landingpage/js/timer.js')?>"></script>
    <!-- //Custom-JavaScript-File-Links -->
				
				<div class="subscribe wthree">
				<h2><span>Subscribe</span> & be the first to know</h2>
					<div class="contact-form">
						<form action="#" method="post">
							<input type="email" value="Email" name="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">	
							<input type="submit" value="Subscribe">	
							<div class="clear"></div>
						</form>
					</div>
				</div>
    </div>
	<div class="clear"></div>
			</div>
		<p class="copy_rights">&copy; 2017 Opulent Coming Soon Widget. All Rights Reserved | Design by  <a href="http://w3layouts.com/"> W3layouts</a></p>
	</div>
				
</body>
</html>
