<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';


/* ================= API ===================== */
$route['api/SetLoginWithFacebook'] = "S3Module/SetLoginWithFacebook";
$route['api/SetLoginWithLine'] = "S3Module/SetLoginWithLine";
$route['api/SetLoginWithApple'] = "S3Module/SetLoginWithApple";
$route['api/GetAllChannelList'] = "S3Module/GetAllChannelList";
$route['api/SetChannelClick'] = "S3Module/SetChannelClick";
$route['api/GetChannelRecentView'] = "S3Module/GetChannelRecentView";
$route['api/SetLoginWithGuest'] = "S3Module/SetLoginWithGuest";
$route['api/SetChipCodeLatLon'] = "S3Module/SetChipCodeLatLon";
$route['api/GetYoutubeCategory'] = "S3Module/GetYoutubeCategory";
$route['api/SetMemberGenderAndBirthday'] = "S3Module/SetMemberGenderAndBirthday";
$route['api/SetMemberDeviceToken'] = "S3Module/SetMemberDeviceToken";

//$route['api/SetYoutubeAPIKeyOverLimit'] = "S3Module/SetYoutubeAPIKeyOverLimit";

$route['api/SetYoutubeViewLogs'] = "S3Module/SetYoutubeViewLogs";

/* set history link log (s3 remote service) */
$route['api/SetHistoryLinkLogs'] = "S3Module/SetHistoryLinkLogs";

/* get history link log (s3 remote service) */
$route['api/GetHistoryLinkLogs'] = "S3Module/GetHistoryLinkLogs";

/* edit history link log (s3 remote service) */
$route['api/EditHistoryLinkLogs'] = "S3Module/EditHistoryLinkLogs";

/* delete history link log (s3 remote service) */
$route['api/DelHistoryLinkLogs'] = "S3Module/DelHistoryLinkLogs";

/* check stream channel (s3 remote service) */
$route['api/CheckStreamChannel'] = "S3Module/CheckStreamChannel" ;

/* check downloadlink  (s3 remote service) */
$route['api/CheckDownloadLink'] = "S3Module/CheckDownloadLink" ;

# INTERNET TV Channel Click
/* check all internet tv channel list  (s3 remote service) */
$route['api/GetInternetTvChannelList'] = "S3Module/GetInternetTvChannelList" ;
$route['api/SetInternetTvChannelClick'] = "S3Module/SetInternetTvChannelClick";
$route['api/GetDLTVYoutubeURL'] = "S3Module/GetDLTVYoutubeURL";
#EOF INTERNET TV Channel Click

/* additional 25.10.18 */
$route['api/GetChannelListByBandType'] = "S3Module/GetChannelListByBandType";

/* api for rating box */
$route['api/SetChannelRating'] =  "S3Module/SetChannelRating";

/* channel advertisement popup */
$route['api/GetChannelAdvertisementMobilePopup'] = "S3Module/GetChannelAdvertisementMobilePopup";

/* true vision premier league*/ 
$route['api/GetPremierLeagueSchedule'] = "S3Module/GetPremierLeagueSchedule"; 

// 
/** boomerang channel  */
$route['api/GetBoomerangChannel'] = "S3Module/GetBoomerangChannel";

$route['api/SetBoomerangChannelClick'] = "S3Module/SetBoomerangChannelClick";

/** case: s3remote service set counting click ( requirment 2021-07-14 ) */
$route['api/SetCountingClick'] = "S3Module/SetCountingClick";
//=========================== Media hub mockup ===============================

$route['media_hub/mock/GetAllHilightPrograms'] = "MediaHubMock/GetAllHilightPrograms";
$route['media_hub/mock/GetDefaultTimeSlots'] = "MediaHubMock/GetDefaultTimeSlots";
$route['media_hub/mock/GetTop10Channels'] = "MediaHubMock/GetTop10Channels";
$route['media_hub/mock/GetHistoriesReservation'] = "MediaHubMock/GetHistoriesReservation";
$route['media_hub/mock/ViewAdvertisement'] = "MediaHubMock/ViewAdvertisement";
$route['media_hub/mock/GetFilterTimeSlots'] = "MediaHubMock/GetFilterTimeSlots";





$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
